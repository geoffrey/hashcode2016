all: 1o 2o 3o ../submission.zip

submission.zip:
	rm submission.zip
	git gc
	zip $@ .git *.py -r

1o: 1
	python reborn.py $<
2o: 2
	python reborn.py $<
3o: 3
	python reborn.py $<

.PHONY: submission.zip 1o 2o 3o
